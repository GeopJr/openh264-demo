
namespace VideoTest {
    [GtkTemplate (ui = "/dev/geopjr/video/window.ui")]
    public class Window : Adw.ApplicationWindow {
        [GtkChild]
        private unowned Gtk.Box box;

        public Window (Gtk.Application app) {
            Object (application: app);

            var file = File.new_for_uri ("https://media.tech.lgbt/media_attachments/files/110/037/642/265/430/439/original/14ce45101cd75adf.mp4");
            var video = new Gtk.Video () {
                vexpand = true
            };
            video.set_file (file);
            box.append(video);
        }
    }
}
