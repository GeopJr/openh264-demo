
namespace VideoTest {
    public class Application : Adw.Application {
        public Application () {
            Object (application_id: "dev.geopjr.video", flags: ApplicationFlags.DEFAULT_FLAGS);
        }

        construct {
            ActionEntry[] action_entries = {
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", {"<primary>q"});
        }

        public override void activate () {
            base.activate ();
            var win = this.active_window;
            if (win == null) {
                win = new VideoTest.Window (this);
            }
            win.present ();
        }
    }
}
